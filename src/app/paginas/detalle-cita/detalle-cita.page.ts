import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import dayGridPlugin from '@fullcalendar/daygrid';
import { Calendar } from '@fullcalendar/core';
import { AlertController } from '@ionic/angular';
import timeGridPlugin from '@fullcalendar/timegrid';
import esLocale from '@fullcalendar/core/locales/es';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
moment.locale('es');
@Component({
  selector: 'app-detalle-cita',
  templateUrl: './detalle-cita.page.html',
  styleUrls: ['./detalle-cita.page.scss'],
})
export class DetalleCitaPage implements OnInit {
  private baseURL = 'https://animatiomx.com/lumi/';
  info = [];
  rol = '';
  idUsuario = '';
  idC = '';
  usuario: any = [];
  calendarioinfo: any = [];
  empresasid : any = [];
  empresasinfo: any = [''];
  recordatorioinfO: any = [];
  idempresa = '';
  Motivo = '';
  fechainicio;
  fechafinal;
  horainicial;
  horafinal;
  dia;
  mes;
  horaa: number;
  minutoa: number;
  horaa1 = '';
  minutoa1 = '';
  horainput = ''
  horainput1 = ''
  fechayhorainicial;
  fechayhorafinal;
  arrayfecha: any = [];
  arrayfecha1: any = [];
  idrecordatorio = '';
  idcita = '';
  direccion = '';
  archivofinal: any;
  archivos: string  []  =  [];
  nombredearchivos: any = [];
  clasemotivo: string = "input form-control";
  clasesel: string = "input form-control";
  claserecor: string = "input form-control";
  claseerrormotivo: string = '';
  claseerrorempresa: string = '';
  claseerrornot: string = '';
  arreglodearchivosbase: any = [];
  sucuO: any = [];
  sucuO2: any = [];
  idSO = '';
  nombreSO = '';
  idEmSel = '';
  idSOseleccionado = '';
  idSucuObraM = '';
  empresabuscador = '';
  tipodearchivo: any = [];
  prospecto = true;
  cprospecto = true;
  csuc = true;
  fechaelegi: any;
  fechaelegi2: any;
  fecha2: any;
  fecha3: any;
  fecha4: any;
  tiempo: any;
  nombreE = '';
  constructor(private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient,
              private subirarchivo: SubirarchivoService,
              public alertController: AlertController) {
                this.route.params.subscribe(parametros => {
                  this.idC = parametros['idC'];
                });
               }

  ngOnInit() {
    console.log(this.idC);
    this.obtnerinfocita();
    this.recordatorio();
    this.obtenerinfoarchivos();
  }

  obtnerinfocita(){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 5, 'idCita': this.idC};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        const idemp = respuesta[0].idEmpresa;
        this.idempresa = respuesta[0].idEmpresa;
        this.Motivo = respuesta[0].Motivo;
        this.fechainicio = respuesta[0].FechaI;
        this.horainicial = respuesta[0].HoraI;
        this.fechafinal = respuesta[0].FechaF;
        this.horafinal = respuesta[0].HoraF;
        this.idSucuObraM = respuesta[0].SucursalObra;
        this.nombreSO = respuesta[0].nombreSO;
        this.nombreE = respuesta[0].Nombre_Empresa;
        this.idrecordatorio = respuesta[0].Recordatorios_idRecordatorios;
        this.fechayhorainicial = (moment(this.fechainicio).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horainicial;

        this.fechayhorafinal = (moment(this.fechafinal).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horafinal;

        const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
        const options: any = { 'caso': 3, 'idEmpresa': idemp};
        const URL: any = this.baseURL + 'calendario.php';
        this.http.post(URL, JSON.stringify(options), headers).subscribe(
          respuesta => {
            this.empresasinfo = respuesta;
            this.direccion = this.empresasinfo[0].Direccion_Empresa;
            this.idEmSel = this.empresasinfo[0].idEmpresas;
          });
        this.obtenSucO2(idemp);
        console.log(this.idempresa);
        console.log(idemp + "holaaaaaaaaaaaaaaaaa");
      });

  }

  editarcita(){
    if (this.Motivo !== ''  && this.idrecordatorio !== '' && this.fechayhorafinal !== '' && this.fechayhorainicial !== ''){
      console.log('Motivo ' + this.Motivo + 'fecha i' + this.fechainicio +
      ' hora i ' + this.horainicial + 'fecha f' + this.fechafinal + ' hora f' + this.horafinal + 'recordatorio' + this.idrecordatorio);
      const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
      const options: any = { 'caso': 8, 'motivo': this.Motivo, 'fechai': this.fechainicio,
      'fechaf': this.fechafinal, 'horai': this.horainicial, 'horaf': this.horafinal,
      'recordatorio': this.idrecordatorio, 'idCita': this.idC, 'idSO2': this.idSucuObraM};
      let fechaInicial = this.fechainicio.concat(this.horainicial.toString());
      const URL: any = this.baseURL + 'calendario.php';
      this.http.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {

          if (this.archivos.length > 0) {
            this.servidorarchivo1(this.idC, 0);
            this.noti(this.idC, fechaInicial, this.nombreE);
          } else {
            this.ngOnInit();
            this.noti(this.idC, fechaInicial, this.nombreE);
          }
        });
        this.redireccion();
    }
  }
  obtenSucO2(idE) {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 16, 'idEmp': idE};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.sucuO2 = respuesta;
        if (this.sucuO2 != null) {
          this.csuc = true;
        }else{
          this.csuc = false;
        }
      });
  }
  servidorarchivo1(idcita, idE) {
    const formData = new FormData();

    for  (var i =  0; i <  this.archivos.length; i++)  {
      formData.append("file[]",  this.archivos[i]);
  }
    formData.append('idcita', idcita);
    formData.append('idtarea', '0');
    formData.append('idEmp', idE);
    this.subirarchivo.enviararchivo(formData).subscribe(
      resp => {
        if (resp.toString() !== '') {
          this.ngOnInit();
        } else {
          console.log('ocurrio un error');
          this.archivos = [];
        }
      },
    );
  }
  obtenerifoempresa(){
    this.cprospecto = false;
    if (this.idempresa !== ''){
      this.clasesel = 'input form-control';
      this.claseerrorempresa = '';
    }
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 3, 'idEmpresa': this.idempresa};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasinfo = respuesta;
        this.direccion = this.empresasinfo[0].Direccion_Empresa;
      });
    this.obtenSucO();

  }

  obtenerifoempresa1(){
    this.prospecto = false;
    this.idSO = '0';
    if (this.idempresa !== ''){
      this.clasesel = 'input form-control';
      this.claseerrorempresa = '';
    }
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 3, 'idEmpresa': this.idempresa};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasinfo = respuesta;
        this.direccion = this.empresasinfo[0].Direccion_Empresa;
      });
    this.obtenSucO();
  }
  obtenSucO() {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 16, 'idEmp': this.idempresa};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.sucuO = respuesta;
        if (this.sucuO != null) {
          this.csuc = true;
        }else{
          this.csuc = false;
        }
      });
  }
  onFileChanged(event) {
    for  (var i =  0; i <  event.target.files.length; i++)  {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = (event.target.files[i].name.substring(event.target.files[i].name.lastIndexOf('.') + 1)).toLowerCase();
      this.tipodearchivo.push(ext);
  }
  // for (let index = 0; index < event.target.files[0].name.length; index++) {
  //   this.nombredearchivos.push(event.target.files[0].name[index]);
  // }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }
  fecha(){
    const fecha = new Date();
    const fechahoy = moment(fecha).format();
    const fechaelegida =  moment(this.fechainicio + ' ' + this.horainicial).format();
    console.log(fechahoy);
    console.log(fechaelegida);
    if (fechahoy > fechaelegida) {
      this.fechayhorainicial = '';
    } else {
      this.fechayhorainicial = (moment(this.fechainicio).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horainicial;
      this.fechaelegi = this.fechainicio.concat('T' + this.horainicial.toString());
      this.fechaelegi2 = new Date(this.fechaelegi);
      // Tiempo es igual a un dia en microsecs
      if (this.idrecordatorio === '1')
      {
        this.tiempo = 1000 *  60 * 5;
      }
      if (this.idrecordatorio === '2')
      {
        this.tiempo = 1000 *  60 * 15;
      }
      if (this.idrecordatorio === '3')
      {
        this.tiempo = 1000 *  60 * 30;
      }
      if (this.idrecordatorio === '4')
      {
        this.tiempo = 1000 *  60 * 60 * 24 * 1;
      }
      if (this.idrecordatorio === '5')
      {
        this.tiempo = 1000 *  60 * 60 * 24 * 2;
      }
      // getTime convierte una fecha a microsegundos
      this.fecha2 = this.fechaelegi2.getTime();
      // Operacion para quitar el tiempo a la fecha asignada
      this.fecha3 = this.fecha2 - this.tiempo;
      console.log(this.fechaelegi);
      console.log(this.fechaelegi2);
      console.log(this.fecha2 + 'Esta es fechat 2 getTime');
      // convirtiendo a date los microsegundis nos da una fecha
      console.log(new Date(this.fecha3) + 'Esta es fechat 3 en date');
    }

    this.fecha1();

  }

  fecha1(){
    const fechainicio =  moment(this.fechainicio + ' ' + this.horainicial).format();
    const fechaelegida =  moment(this.fechafinal + ' ' + this.horafinal).format();
    if (fechainicio > fechaelegida) {
      this.fechayhorafinal = '';
    } else {
      this.fechayhorafinal = (moment(this.fechafinal).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horafinal;
    }

  }
  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }
  recordatorio(){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 2};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.recordatorioinfO = respuesta;
      });
  }

  async eliminararchivo(id){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar archivo',
      message: '<strong>¿Desea eliminar este archivo?</strong>!!!',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            console.log('Se borro');
            const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
            const options: any = { 'caso': 7, 'idArchivo': id};
            const URL: any = this.baseURL + 'calendario.php';
            this.http.post(URL, JSON.stringify(options), headers).subscribe(
              respuesta => {
                this.obtenerinfoarchivos();
              });
          }
        }
      ]
    });
    await alert.present();
  }
  obtenerinfoarchivos(){

    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 6, 'idCita': this.idC};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.arreglodearchivosbase = respuesta;
      });
  }
  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  async redireccion(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Se ha modificado exitosamente!',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Se borro');
            this.router.navigate(['/tabs/tab1']);
          }
        }
      ]
    });
    await alert.present();
  }
  async noti(id,f,e) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title: "Recordatorio de tu cita",
          body: "Tienes una cita asignada el día de hoy",
          id: id,
          schedule: { at: new Date(this.fecha3 ) },
          sound: null,
          attachments: null,
          extra: {
            route: ['/tabs/tab1/mapa-cita/'+String(f)+'/'+e+'/'+id],
          },
          iconColor: '#BD008C',
        }
      ]
    });
    console.log('scheduled notifications', notifs);
    console.log(new Date());
    console.log('Se ha guardado tu notificacion con el id: ' + id);
  }
}
