import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from "@ionic/angular";
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
import * as moment from 'moment';

@Component({
  selector: "app-nueva-citatar",
  templateUrl: "./nueva-citatar.page.html",
  styleUrls: ["./nueva-citatar.page.scss"],
})
export class NuevaCitatarPage implements OnInit {
  private baseURL = "https://animatiomx.com/lumi/";
  private baseURL2 = "https://animatiomx.com/lumi/";
  prospecto = "";
  idSO = "";
  sucuO: any = [];
  empresa: any = [];
  direccion = "";
  direccion2 = "";
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  Motivo = "";
  fechainicio;
  fechafinal;
  horainicial;
  horafinal;
  fechayhorainicial;
  fechayhorafinal;
  recordatorioinfO: any = [];
  idrecordatorio = "";
  idempresa = "";
  cprospecto = true;
  store: any;
  empresasinfo: any = [""];
  csuc = true;
  idcita = "";
  nombreE = "";
  nombreSuc = "";
  nombrePara = "";
  checkin = "";
  negado = "";
  idcheck = "0";
  iduser;
  idemp;
  idsuc;
  idUsuario;
  fechaelegi: any;
  fechaelegi2: any;
  fecha2: any;
  fecha3: any;
  fecha4: any;
  tiempo: any;
info = [];

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private subirarchivo: SubirarchivoService,
    private router: Router,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertController: AlertController
  ) {
     const info1 = localStorage.getItem("idL");
     const info2 = info1.replace("[", "").replace("]", "").replace("", "");
     this.info = info2.split(",");
     this.idUsuario = this.info[0].replace('"', "").replace('"', "");
     console.log(this.idUsuario);

     this.route.params.subscribe((parametros) => {
      this.iduser = parametros["idu"];
      this.idemp = parametros["ide"];
      this.idsuc = parametros["ids"];
    });
  }

  ngOnInit() {
    this.obtenSucO(this.idemp);
    this.obtenerempresa();
    this.obteneruser();
    this.recordatorio();
  }

  obtenerempresa() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 9, idE: this.idemp };
    const URL: any = this.baseURL2 + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.empresa = respuesta;
        this.nombreE = this.empresa[0].Nombre_Empresa;
        this.direccion2 = this.empresa[0].Direccion_Empresa;
      });
  }

  obteneruser() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 10, idU: this.iduser };
    const URL: any = this.baseURL2 + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.nombrePara = respuesta[0].Nombre;
        console.log(this.nombrePara);
      });
  }

  recordatorio() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 2 };
    const URL: any = this.baseURL + "calendario.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.recordatorioinfO = respuesta;
      });
  }

  obtenSucO(idE) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 16, idEmp: idE };
    const URL: any = this.baseURL + "detalle_cita.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.sucuO = respuesta;
        if (this.sucuO != null) {
          this.csuc = true;
          this.infosuc();
          this.nombreSuc = this.sucuO[0].nombreSO;
          this.idSO = this.sucuO[0].idSO;
        } else {
          this.csuc = false;
        }
      });
  }

  infosuc() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 6, sucursal: this.idSO };
    const URL: any = this.baseURL + "check.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        if (respuesta == null) {
          this.direccion = "";
        } else {
          this.direccion = respuesta[0].direccion;
        }
      });
  }

  fecha() {
    const fecha = new Date();
    const fechahoy = moment(fecha).format();
    const fechaelegida = moment(
      this.fechainicio + " " + this.horainicial
    ).format();
    console.log(fechahoy);
    console.log(fechaelegida);
    if (fechahoy > fechaelegida) {
      this.fechayhorainicial = "";
    } else {
      this.fechayhorainicial =
        moment(this.fechainicio).format("dddd DD [de] MMMM [de] YYYY") +
        " " +
        this.horainicial;
      this.fechaelegi = this.fechainicio.concat('T' + this.horainicial.toString());
      this.fechaelegi2 = new Date(this.fechaelegi);
    // Tiempo es igual a un dia en microsecs
      if (this.idrecordatorio === '1')
    {
      this.tiempo = 1000 *  60 * 5;
    }
      if (this.idrecordatorio === '2')
    {
      this.tiempo = 1000 *  60 * 15;
    }
      if (this.idrecordatorio === '3')
    {
      this.tiempo = 1000 *  60 * 30;
    }
      if (this.idrecordatorio === '4')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 1;
    }
      if (this.idrecordatorio === '5')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 2;
    }
    // getTime convierte una fecha a microsegundos
      this.fecha2 = this.fechaelegi2.getTime();
    // Operacion para quitar el tiempo a la fecha asignada
      this.fecha3 = this.fecha2 - this.tiempo;
      console.log(this.fechaelegi);
      console.log(this.fechaelegi2);
      console.log(this.fecha2 + 'Esta es fechat 2 getTime');
    // convirtiendo a date los microsegundis nos da una fecha
      console.log(new Date(this.fecha3) + 'Esta es fechat 3 en date');
    }

    this.fecha1();
  }

  async noti(id,f,e) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title: "Recordatorio de tu cita",
          body: "Tienes una cita asignada el día de hoy",
          id: id,
          schedule: { at: new Date(this.fecha3 ) },
          sound: null,
          attachments: null,
          extra: {
            route: ['/tabs/tab1/mapa-cita/'+String(f)+'/'+e+'/'+id],
          },
          iconColor: '#BD008C',
        }
      ]
    });
    console.log('scheduled notifications', notifs);
    console.log(new Date());
    console.log('Se ha guardado tu notificacion con el id: ' + id);
  }

  fecha1() {
    const fechainicio = moment(
      this.fechainicio + " " + this.horainicial
    ).format();
    const fechaelegida = moment(
      this.fechafinal + " " + this.horafinal
    ).format();
    if (fechainicio > fechaelegida) {
      this.fechayhorafinal = "";
    } else {
      this.fechayhorafinal =
        moment(this.fechafinal).format("dddd DD [de] MMMM [de] YYYY") +
        " " +
        this.horafinal;
    }
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  async crearcita() {
if (this.idsuc == 0) {
      let loader = await this.loadingCtrl.create({
        message: "Creando cita",
      });
      await loader.present();
      // console.log('direccion '+ this.direccion + 'Motivo '+ this.Motivo + 'fecha i'+ this.fechainicio+
      // ' hora i '+ this.horainicial+ 'fecha f'+ this.fechafinal + ' hora f' + this.horafinal+
      // 'usuario'+ this.store + 'recordatorio'+ this.idrecordatorio+ 'empresa' + this.idempresa+'sucursal' + this.idSO);
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = {
        caso: 10,
        direccion: this.direccion2,
        motivo: this.Motivo,
        fechai: this.fechainicio,
        fechaf: this.fechafinal,
        horai: this.horainicial,
        horaf: this.horafinal,
        idSO: this.idsuc,
        usuario: this.iduser,
        recordatorio: this.idrecordatorio,
        empresa: this.idemp,
        idcheck: this.idcheck,
      };
      let fechaInicial = this.fechainicio.concat(this.horainicial.toString());
      const URL: any = this.baseURL + "check.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          const idcita = respuesta.toString();

          if (this.archivos.length === 0) {
            loader.dismiss();
            this.inicializarvariables();
            this.noti(idcita, fechaInicial, this.nombreE);
            this.Confirma();
          } else {
            const formData = new FormData();
            for (var i = 0; i < this.archivos.length; i++) {
              formData.append("file[]", this.archivos[i]);
            }
            formData.append("idcita", idcita);
            formData.append("idtarea", "0");
            formData.append("idEmp", this.idemp);

            this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
              if (resp.toString() !== "") {
                loader.dismiss();
                this.inicializarvariables();
                this.noti(idcita, fechaInicial, this.nombreE);
                this.Confirma();
              } else {
                loader.dismiss();
                this.mensaje("ocurrio un error intente nuevamente");
              }
            });
          }
        });
} else {
      let loader = await this.loadingCtrl.create({
        message: "Creando cita",
      });
      await loader.present();
      // console.log('direccion '+ this.direccion + 'Motivo '+ this.Motivo + 'fecha i'+ this.fechainicio+
      // ' hora i '+ this.horainicial+ 'fecha f'+ this.fechafinal + ' hora f' + this.horafinal+
      // 'usuario'+ this.store + 'recordatorio'+ this.idrecordatorio+ 'empresa' + this.idempresa+'sucursal' + this.idSO);
      const headers: any = new HttpHeaders({
        "Content-Type": "application/json",
      });
      const options: any = {
        caso: 10,
        direccion: this.direccion,
        motivo: this.Motivo,
        fechai: this.fechainicio,
        fechaf: this.fechafinal,
        horai: this.horainicial,
        horaf: this.horafinal,
        idSO: this.idsuc,
        usuario: this.iduser,
        recordatorio: this.idrecordatorio,
        empresa: this.idemp,
        idcheck: this.idcheck,
      };
      let fechaInicial = this.fechainicio.concat(this.horainicial.toString());
      const URL: any = this.baseURL + "check.php";
      this.http
        .post(URL, JSON.stringify(options), headers)
        .subscribe((respuesta) => {
          const idcita = respuesta.toString();

          if (this.archivos.length === 0) {
            loader.dismiss();
            this.inicializarvariables();
            this.noti(idcita, fechaInicial, this.nombreE);
            this.Confirma();
          } else {
            const formData = new FormData();
            for (var i = 0; i < this.archivos.length; i++) {
              formData.append("file[]", this.archivos[i]);
            }
            formData.append("idcita", idcita);
            formData.append("idtarea", "0");
            formData.append("idEmp", this.idemp);

            this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
              if (resp.toString() !== "") {
                loader.dismiss();
                this.inicializarvariables();
                this.noti(idcita, fechaInicial, this.nombreE);
                this.Confirma();
              } else {
                loader.dismiss();
                this.mensaje("ocurrio un error intente nuevamente");
              }
            });
          }
        });
}

  }

  async mensaje(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      cssClass: "toastcurva",
    });

    await toast.present();
  }

  inicializarvariables() {
    this.nombredearchivos = [];
    this.archivos = [];
    this.tipodearchivo = [];
  }

  async Confirma() {
    const alert = await this.alertController.create({
      cssClass: "alertclass",
      header: "Se creó con exito!",
      buttons: [
        {
          text: "Entendido",
          handler: () => {
            this.router.navigate(["tabs/tab2/nueva-tarea"]);
          },
        },
      ],
    });

    await alert.present();
  }
  // checkout() {
  //   this.router.navigate(['/tabs/tab1/chek-out/' + this.idcita + '/' + this.checkin + '/' + this.nombreE + '/' + this.idcheck]);
  // }
}

