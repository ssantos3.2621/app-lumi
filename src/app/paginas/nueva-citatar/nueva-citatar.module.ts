import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevaCitatarPageRoutingModule } from './nueva-citatar-routing.module';

import { NuevaCitatarPage } from './nueva-citatar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevaCitatarPageRoutingModule
  ],
  declarations: [NuevaCitatarPage]
})
export class NuevaCitatarPageModule {}
