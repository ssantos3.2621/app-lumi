
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

declare var google:any;

@Injectable({
    providedIn: 'root'
})
export class GooglemapsService{

    apiKey = environment.ApiKeyGoogleMaps;
    mapsLoaded = false;

    constructor() {}

    int(renderer: any, document: any): Promise<any>{
        return new Promise((resolve) =>{

            if (this.mapsLoaded){
                console.log('google is preview loaded');
                resolve(true);
                return;
            }

            const scrpt = renderer.createElement('script');
            scrpt.id = 'googleMaps';

            window['mapInit'] = () => {
                this.mapsLoaded = true;
                if (google) {
                    console.log('google is loaded');
                    
                } else {
                    console.log('google is not Defined');
                }
                resolve(true);
                return;
            }

            if (this.apiKey) {
                scrpt.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
            } else {
                scrpt.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
            }

            renderer.appendChild(document.body, scrpt);

        })
    }

}