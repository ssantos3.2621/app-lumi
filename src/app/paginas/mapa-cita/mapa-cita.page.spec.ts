import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MapaCitaPage } from './mapa-cita.page';

describe('MapaCitaPage', () => {
  let component: MapaCitaPage;
  let fixture: ComponentFixture<MapaCitaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaCitaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MapaCitaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
