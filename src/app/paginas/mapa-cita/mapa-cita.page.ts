import { Component, OnInit, ElementRef, ViewChild, Renderer2, Inject,NgZone, OnDestroy  } from '@angular/core';
import { ActionSheetController, LoadingController, Platform,ToastController } from '@ionic/angular';
import { Media,MediaObject } from '@ionic-native/media/ngx';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
//import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { BackgroundGeolocation, BackgroundGeolocationConfig,BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/ngx';
import { Plugins } from '@capacitor/core';
const {Geolocation} = Plugins;
const { LocalNotifications } = Plugins;
declare var google;
import * as moment from 'moment';
import 'moment/locale/es';

import { GooglemapsService } from './googlemaps.service';
import { DOCUMENT } from '@angular/common';
import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';

@Component({
  selector: 'app-mapa-cita',
  templateUrl: './mapa-cita.page.html',
  styleUrls: ['./mapa-cita.page.scss'],
})
export class MapaCitaPage implements OnInit, OnDestroy {
  private baseURL = 'https://animatiomx.com/lumi/';
  map = null;
  latitud: number;
  longitud: number;
  total: string = 'Calculando...';
  latitudi;
  longitudi;
  contadorposicion;
  contadornotificacion;
  audio: MediaObject;
  check: boolean = false;
  metros = 0;
  fechahoy = moment().format("YYYY-MM-DD  h:mm a");
  fechacita ='';
  idcita = '';
  infocita: any = [];
  direccion = '';
  fecha = '';
  foto;
  motivo = '';
  horaI = '';
  horaF = '';
  fechaI = '';
  idE = '';
  status = '';
  vermasdetalle: boolean = false;
  visitas: any = [];
  nombreempresa = '';
  verseguimiento: boolean = false;
  iniciarseguimiento: boolean = false;
  estatuscita = '';
  archivosadjuntos: any = [];

  config: BackgroundGeolocationConfig = {
    desiredAccuracy: 10,
    stationaryRadius: 20,
    distanceFilter: 30,
    debug: false, //  Esto hace que el dispositivo emita sonidos cuando lanza un evento de localización
    stopOnTerminate: false, // Si pones este en verdadero, la aplicación dejará de trackear la localización cuando la app se haya cerrado.
    //Estas solo están disponibles para Android
    locationProvider: 1, //Será el proveedor de localización. Gps, Wifi, Gms, etc...
    startForeground: true,
    interval: 1000, //El intervalo en el que se comprueba la localización.
    fastestInterval: 1000, //Este para cuando está en movimiento.
    activitiesInterval: 1000, //Este es para cuando está realizando alguna actividad con el dispositivo.
  };

  market: any;
  infowindows: any
  positionSet: any;
  @ViewChild('mapa') divMap: ElementRef;

  constructor(
    public actionSheetController: ActionSheetController,
     public loading: LoadingController,
     private media: Media,
     private router: Router,
     private route: ActivatedRoute,
     private http: HttpClient,
     private plt: Platform,
     public toastCtrl: ToastController,
     //private backgroundMode: BackgroundMode,
     private backgroundGeolocation: BackgroundGeolocation,

     private renderer: Renderer2,
     @Inject(DOCUMENT) private document,
     private googlemapsService: GooglemapsService,
     private nativeGeocoder: NativeGeocoder,
     public zone: NgZone
     ) {
      this.backgroundGeolocation.configure(this.config)
      .then(() => {
        this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
          this.latitud = location.latitude;
          this.longitud = location.longitude;
          this.cordenadasiniciales();
        // Es muy importante llamar al método finish() en algún momento para que se le notifique al sistema que hemos terminado y que libere lo que tenga que liberar,
        // Si no se hace, la aplicación dará error por problemas de memoria.
        //this.backgroundGeolocation.finish(); // SOLO PARA IOS
        });
      });
   }

   ngOnInit() {
    // console.log('ngOnInit')
    var ruta = 'https://animatiomx.com/lumi/Archivos/sonido.mp3';
    this.audio = this.media.create(ruta);
    this.fechacita = this.route.snapshot.paramMap.get('fc');
    this.idcita = this.route.snapshot.paramMap.get('idc');
    this.nombreempresa = this.route.snapshot.paramMap.get('nemp');
    this.init();
  }

  async init(){    
    this.googlemapsService.int(this.renderer, this.document).then( () =>{
      this.cargandomapa();
    }).catch( (err) =>{
      console.log(err);
    })
  }

  ionViewWillEnter() {

    // console.log('ionViewWillEnter')
  }

  ionViewDidEnter(){
    //  console.log('ionViewDidEnter')

    this.check = false;
    this.metros = 0;
    this.total = 'Calculando...'
    this.vermasdetalle = false;
    this.iniciarseguimiento = false;
    // console.log('fechacita'+ this.fechacita + 'fechahoy' + this.fechahoy)
    if (moment(this.fechacita).format("YYYY-MM-DD  h:mm") >= moment(this.fechahoy).format("YYYY-MM-DD  h:mm")) {
      this.verseguimiento = true;
    }else{
      this.mensajealerta("La fecha de esta cita ya venció, cambie la fecha o comuníquese con su coordinador")
    }
    setTimeout(() => {
      this.obtenerubicacion1();
      this.obtenervisitas();
      this.obtenerarchivos();
     }, 1500);

  }

  ngOnDestroy(): void{
    // console.log('ionViewDidLeave')
    clearInterval(this.contadornotificacion);
    clearInterval(this.contadorposicion);
    this.backgroundGeolocation.stop();
  }


  ionViewDidLeave(){
    // console.log('ionViewDidLeave')
    clearInterval(this.contadornotificacion);
    clearInterval(this.contadorposicion);
    this.backgroundGeolocation.stop();
  }

  ionViewWillLeave(){
    // console.log('ionViewDidLeave')
    clearInterval(this.contadornotificacion);
    clearInterval(this.contadorposicion);
    this.backgroundGeolocation.stop();
  }

  async cargandomapa() {
    const cargando = await this.loading.create({
      message: "Cargando mapa..."
    });

    await cargando.present();

    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 0 , 'idCitas': this.idcita};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.infocita = respuesta;
        this.direccion = respuesta[0].Direccion;
        this.foto = respuesta[0].Foto;
        this.status = respuesta[0].Status_Empresa;
        this.motivo = respuesta[0].Motivo;
        this.fechaI = respuesta[0].FechaI;
        this.fecha = (moment(respuesta[0].FechaI).format('dddd DD [de] MMMM [de] YYYY'));
        this.horaI = respuesta[0].HoraI;
        this.horaF = respuesta[0].HoraF;
        this.idE= respuesta[0].idEmpresa;
        this.estatuscita = respuesta[0].Status;
        if (this.estatuscita === '2') {
          cargando.dismiss();
          this.router.navigate(['/tabs/tab1/chek-in/'+ this.idcita]);
        } 
        else{
          
          // console.log(this.direccion);
          if (this.plt.is('cordova')) {
            let options: NativeGeocoderOptions = {
              useLocale: true,
              maxResults: 5
            };
            this.nativeGeocoder.forwardGeocode(this.direccion, options)
              .then((result: NativeGeocoderResult[]) => {
                this.zone.run(() => {
                  this.latitudi = result[0].latitude;
                  this.longitudi = result[0].longitude;
                  let latLng = new google.maps.LatLng(this.latitudi, this.longitudi);
      
                  let mapOptions = {
                    center: latLng,
                    zoom: 15,
                    disableDefaultUI: true,
                    streetViewControl: true,
                    mapTypeControl: false,
                    scaleControl: true,
                    zoomControl: true,
                    fullscreenControl: true
                  }
          
                  this.map = new google.maps.Map(this.divMap.nativeElement, mapOptions);
                  this.market = new google.maps.Marker({
                    map: this.map,
                    animation: google.maps.Animation.DROP,
                    draggable: false
                  });
          
                  cargando.dismiss();

                  //this.clickHandleEvent();
                  this.infowindows = new google.maps.InfoWindow();
                  let position = {
                    lat: this.latitudi,
                    lng: this.longitudi
                  }
                  this.addMarker(position);
                  this.setInfoWindow(this.market, 'Ubicación de destino', this.nombreempresa)
                })
              })
              .catch((error: any) => console.log(error));
          } else {
            let geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': this.direccion }, (results, status) => {
              if (status == google.maps.GeocoderStatus.OK) {
                this.zone.run(() => {
                  this.latitudi = Number(results[0].geometry.location.lat());
                  this.longitudi = Number(results[0].geometry.location.lng());
                  // console.log(results[0].geometry.location.lat(),results[0].geometry.location.lng());
                  let latLng = new google.maps.LatLng(this.latitudi, this.longitudi);
      
                  let mapOptions = {
                    center: latLng,
                    zoom: 15,
                    disableDefaultUI: true,
                    streetViewControl: true,
                    mapTypeControl: false,
                    scaleControl: true,
                    zoomControl: true,
                    fullscreenControl: true
                  }
          
                  this.map = new google.maps.Map(this.divMap.nativeElement, mapOptions);
                  this.market = new google.maps.Marker({
                    map: this.map,
                    animation: google.maps.Animation.DROP,
                    draggable: false
                  });
          
                  cargando.dismiss();
                  //this.clickHandleEvent();
                  this.infowindows = new google.maps.InfoWindow();
                  let position = {
                    lat: this.latitudi,
                    lng: this.longitudi
                  }
                  this.addMarker(position);
                  this.setInfoWindow(this.market, 'Ubicación de destino', this.nombreempresa)
                  })
                  } else {
                  alert('Error - ' + results + ' & Status - ' + status)
              }
            });
          }
        }
      });
  }

  addMarker(position: any):void{
    let latLng = new google.maps.LatLng(position.lat, position.lng);

    this.market.setPosition(latLng);
    this.map.panTo(position);
    this.positionSet = position;
  }

  setInfoWindow(marker: any, titulo: string, subtitulo: string){
    const contentString = '<div id="contentInsideMap">' +
                          '<div>' +
                          '</div>' +
                          '<p style="font-weight: bold; margin-bottom: 5px;">' + titulo +
                          '<div id="bodyContent">' +
                          '<P class="normal m-0">'
                          + subtitulo + '</p>'+
                          '</div>'+
                          '</div>';
    this.infowindows.setContent(contentString);
    this.infowindows.open(this.map, marker);
  }

  async mensaje1(msg){
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top',
      cssClass: "toastcurva",
    });

    await toast.present();
  }
  cordenadasiniciales(){
    if (this.fechahoy < this.fechacita) {
        this.obtenerubicacion();
    } else {
      // console.log('la fecha no es hoy');
    }
  }

  async obtenerubicacion() {
    this.calculateDistance(this.longitud, this.longitudi, this.latitud, this.latitudi);
  }

  async obtenerubicacion1() {
    const posicion = await Geolocation.getCurrentPosition();
    this.latitud = posicion.coords.latitude;
    this.longitud = posicion.coords.longitude;
    this.calculateDistance(this.longitud, this.longitudi, this.latitud, this.latitudi);
   }

  calculateDistance(lon1, lon2, lat1, lat2){
    
    let total = '0 KM';

    let posicion1 = new google.maps.LatLng(lat1, lon1);
    let posicion2 = new google.maps.LatLng(lat2, lon2);
    

    // console.log('posicion i'+posicion1);
    // console.log('posicion f'+posicion2);

    var distancia = google.maps.geometry.spherical.computeDistanceBetween(posicion1, posicion2);
    var dis = distancia/ 1000;

    // console.log('--------------------------------------------------Distancia en metros:' + distancia);
    // console.log('--------------------------------------------------Distancia en kilómetros:' + (distancia  * 0.001));
    // console.log('--------------------------------------------------Distancia en metros sin decimales:' + Math.trunc(distancia));

    var metros1 = 0;

    if ( Number(Math.trunc(distancia)) < 1000) {

      const metros = Math.trunc(distancia);

      if (Number(metros) < 300) {
        if (this.iniciarseguimiento === true) {
          this.notificacion();
        }
        this.total = metros + ' m';
        metros1 = Number(metros);
        this.metros = metros1;
        if (metros1 <= 299) {
          if (moment(this.fechacita).format("YYYY-MM-DD  h:mm a") >= moment(this.fechahoy).format("YYYY-MM-DD  h:mm a")) {
          this.check = true;
          }
          else{
            // this.mensajealerta("La fecha de esta cita ya venció, cambie la fecha o comuníquese con su coordinador")
            // console.log("---------------------------------------------")
          }
          if (metros1 >= 50 && metros1 <= 100) {
            this.audio.play();
          }
        }else{
          this.check = false;
        }
      } else {
        if (this.iniciarseguimiento === true) {
          this.notificacion();
        }
        this.total = metros + ' m';
        metros1 =Number(metros);
        this.metros = metros1;
      }
      
    } else {
      this.total = String(dis).substr(0,4)+ ' km';
      this.check = false;
      this.metros = 100;
    }

    // if (dis < 1) {

    //   const distancia = String(dis).split('.');

    //   const metros = distancia[1].substr(0,3);

    //   if (Number(metros) < 400) {
    //     if (this.iniciarseguimiento === true) {
    //       this.notificacion();
    //     }
    //     this.total = metros.substr(-2) + ' m';
    //     metros1 = Number(metros.substr(-2));
    //     this.metros = metros1;
    //     if (metros1 <= 399) {
    //       if (moment(this.fechacita).format("YYYY-MM-DD  h:mm a") >= moment(this.fechahoy).format("YYYY-MM-DD  h:mm a")) {
    //       this.check = true;
    //       }
    //       else{
    //         // this.mensajealerta("La fecha de esta cita ya venció, cambie la fecha o comuníquese con su coordinador")
    //         // console.log("---------------------------------------------")
    //       }
    //       if (metros1 >= 280) {
    //         this.audio.play();
    //       }
    //     }else{
    //       this.check = false;
    //     }
    //   } else {
    //     if (this.iniciarseguimiento === true) {
    //       this.notificacion();
    //     }
    //     this.total = metros + ' m';
    //     metros1 =Number(metros);
    //     this.metros = metros1;
    //   }

    // } else {
    //   this.total = String(dis).substr(0,4)+ ' km';
    //   this.check = false;
    //   this.metros = 100;
    // }
}

  notificacion(){
    //this.total = this.total;
    //(<HTMLInputElement>document.getElementById('totalfinal')).value = this.total;
    this.iniciarseguimiento = true;
      LocalNotifications.schedule({
        notifications: [
          {
            title: this.total,
            body: "Para llegar a tu destino",
            id: 1,
            sound: null,
            attachments: null,
            extra: {
              route: ['/tabs/tab1/mapa-cita/'+String(this.fechacita)+'/'+this.nombreempresa+'/'+this.idcita],
            },
            icon: 'assets/icon/lumi.png',
            iconColor: '#BD008C',
          }
        ]
      });
  }

  obtenervisitas() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0.1, 'idCitas': this.idcita, 'fecha': this.fechaI, 'idEmp':this.idE };
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.visitas = respuesta;
      });
  }

  obtenerarchivos() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 7, 'idCitas': this.idcita };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.archivosadjuntos = respuesta;
      });
  }

  expandircard(){
    if (!this.vermasdetalle) {
      this.vermasdetalle = true
    } else {
      this.vermasdetalle = false
    }
  }

  iralpunto(){
    // console.log('notificacion ir al punto');
    LocalNotifications.schedule({
      notifications: [
        {
          title: this.total,
          body: "Para llegar a tu destino",
          id: 1,
          sound: null,
          attachments: null,
          extra: {
            route: ['/tabs/tab1/mapa-cita/'+String(this.fechacita)+'/'+this.nombreempresa+'/'+this.idcita],
          },
          icon: 'assets/icon/lumi.png',
          iconColor: '#BD008C',
        }
      ]
    });
    this.iniciarseguimiento = true;
    this.backgroundGeolocation.start();
  }

  notificacionprueba(){
    // console.log('notificacion');
    
    LocalNotifications.schedule({
      notifications: [
        {
          title: this.total,
          body: "Para llegar a tu destino",
          id: 1,
          sound: null,
          attachments: null,
          extra: {
            route: ['/tabs/tab1/mapa-cita/'+String(this.fechacita)+'/'+this.nombreempresa+'/'+this.idcita],
          },
          icon: '/assets/icon/lumi.png',
          iconColor: '#BD008C',
        }
      ]
    });
  }

  checkin(){
    const horaactual = moment().format('HH:mm');
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 0, 'idCitas': this.idcita, 'HoraInicio': horaactual };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        try {
          this.backgroundGeolocation.stop();
          clearInterval(this.contadornotificacion);
          clearInterval(this.contadorposicion);
          this.backgroundGeolocation.stop();
          this.router.navigate(['/tabs/tab1/chek-in/'+ this.idcita]);
        } catch (error) {
          console.log('hubo un error' + error);
        }
      });
  }

  eliminaArchivos(id) {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 7, 'idArchivo': id};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
      this.obtenerarchivos();
    });
  }

  async mensaje(asesor,motivo) {
    let toast = await this.toastCtrl.create({
      message: 'Asesor: '+asesor+'\nMotivo: '+motivo,
      duration: 3000,
      position: 'top',
      cssClass: "toastcurva",
    });

    await toast.present();

}

async mensajealerta(motivo) {
  let toast = await this.toastCtrl.create({
    message: motivo,
    duration: 4000,
    position: 'top',
    cssClass: "toastcurva",
  });

  await toast.present();

}

}