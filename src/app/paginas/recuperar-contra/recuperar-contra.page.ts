import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../services/auth.service';
import { AlertController, ToastController } from '@ionic/angular';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-recuperar-contra',
  templateUrl: './recuperar-contra.page.html',
  styleUrls: ['./recuperar-contra.page.scss'],
})
export class RecuperarContraPage implements OnInit {
  vista = 1;
  private baseURL = 'https://animatiomx.com/lumi/';
  loginForm: FormGroup;
  loginForm1: FormGroup;
  loading = false;
  submitted = false;
  submitted1 = false;
  returnUrl: string;
  returnUrl1: string;
  error = '';
  password_type: string = 'password';
  password_type1: string = 'password';
  visible = false;
  vercontrasena = false;
  visible1 = false;
  vercontrasena1 = false;
  email='';

  mostrariconover = false;
  mostrariconover1 = false;
  tipodeinput = 'password';
  iconodeinput = 'eye';
  tipodeinput1 = 'password';
  iconodeinput1  = 'eye';

  @ViewChild("correo", { static: false }) correo: any;
  @ViewChild("pass1", { static: false }) pass1: any;
  @ViewChild("pass2", { static: false }) pass2: any;

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    public toastCtrl: ToastController,
    private http: HttpClient) { }

  ngOnInit() {
    this.vista = 1;
  }

  onSubmit() {
    if (this.correo.value=='') {
      this.presentToast('debe introducir su correo');
        return;
    }else{
      const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
      const options: any = { 'caso': 1, 'email': this.correo.value };
      const URL: any = this.baseURL + 'acceso.php';
      this.http.post(URL, JSON.stringify(options), headers).subscribe(
        respuesta => {
          if (respuesta.toString() === '0')
          {
            this.presentToast('El usuario ' + this.correo.value + ' no esta registrado');
          }
           if (respuesta.toString() === '1')
           {
            this.vista = 2;
            this.email = this.correo.value;
           }
        });
    }
}

onSubmit1() {
  console.log(this.pass1.value,this.pass2.value);
  if (this.pass1.value=='') {
    this.presentToast('debe introducir una contraseña');
      return;
  } else if (this.pass1.value.length < 8) {
    this.presentToast('su contraseña debe contener almenos 8 caracteres');
  } else if (this.pass1.value != this.pass2.value) {
    this.presentToast('su nueva contraseña no coicide');
  }else{
        const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 2, 'email': this.email, 'Password1' : this.pass1.value };
        const URL: any = this.baseURL + 'acceso.php';
        this.http.post(URL, JSON.stringify(options), headers).subscribe(
          respuesta => {
            if (respuesta.toString() === '0')
            {
              this.presentToast('hubo un error intentelo mas tarde');
            }
             if (respuesta.toString() === '1')
             {
               this.presentToast('su contraseña se cambio correctamentte');
               this.router.navigate(['/login']);
             }
          });

  }

}

// iniciar(user,pass) {
//   this.loading = true;
//   this.authenticationService.login(user, pass)
//       .pipe(first())
//       .subscribe(
//           data => {
//               var info = data.toString();
//               if(info !== '0' && info !== 'El usuario no esta registrado'){

//                 this.router.navigate([this.returnUrl]);
//                 this.authenticationService.setUser(data);
//                 const token = data;

//               }

//           });
// }

  vercontra(event) {
    const val = event.target.value;
    this.pass1.value = val;
    // console.log(this.pass1.value);
    if (this.pass1.value === '') {
      this.mostrariconover = false;
    } else {
      this.mostrariconover = true;
    }
  }

  vercontra1(event) {
    const val = event.target.value;
    this.pass2.value = val;
    // console.log(this.pass2.value);
    if (this.pass2.value === '') {
      this.mostrariconover1 = false;
    } else {
      this.mostrariconover1 = true;
    }
  }
  togglePasswordMode() {
    this.tipodeinput = this.tipodeinput === 'text' ? 'password' : 'text';
    this.iconodeinput = this.iconodeinput === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter() {
    this.mostrariconover = false;
  }

  togglePasswordMode1() {
    this.tipodeinput1 = this.tipodeinput1 === 'text' ? 'password' : 'text';
    this.iconodeinput1 = this.iconodeinput1 === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter1() {
    this.mostrariconover1 = false;
  }

borrar(){
  this.error = '';
}

  // metodo para mandar mensajes como toast

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      cssClass: 'my-custom-class',
    });

    await toast.present();
  }


}
