import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
  NgForm,
} from "@angular/forms";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { AlertController } from '@ionic/angular';

@Component({
  selector: "app-cambiar-contra",
  templateUrl: "./cambiar-contra.page.html",
  styleUrls: ["./cambiar-contra.page.scss"],
})
export class CambiarContraPage implements OnInit {
  private baseURL = "https://animatiomx.com/lumi/";
  loginForm: FormGroup;
  loginForm1: FormGroup;
  loading = false;
  submitted = false;
  submitted1 = false;
  returnUrl: string;
  returnUrl1: string;
  error = "";
  error2 = "";
  password_type: string = "password";
  password_type1: string = "password";
  password_type2: string = "password";
  visible = false;
  vercontrasena = false;
  visible1 = false;
  visible2 = false;
  vercontrasena1 = false;
  vercontrasena2 = false;
  vista = 1;
  idUsuario = "";
  info = [];
  cambio: any = [];

  mostrariconover = false;
  mostrariconover1 = false;
  mostrariconover2 = false;

  tipodeinput = 'password';
  iconodeinput = 'eye';

  tipodeinput1 = 'password';
  iconodeinput1 = 'eye';

  iconodeinput2 = 'eye';
  tipodeinput2 = 'password';


  @ViewChild("pass1", { static: false }) pass1: any;
  @ViewChild("pass2", { static: false }) pass2: any;
  @ViewChild("pass3", { static: false }) pass3: any;

  constructor(
    public alertController: AlertController,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) {
    this.route.params.subscribe((parametros) => {
      this.idUsuario = parametros["id"];
    });
  }

  ngOnInit() {}

  cambiarcontra() {
    console.log(this.pass1.value, this.pass2.value, this.pass3.value, this.pass2.value.length);
    if (this.pass1.value=='') {
      this.presentAlert5();
    }else if (this.pass2.value != this.pass3.value) {
      console.log('su nueva contraseña no coicide');
      this.presentAlert();
    } else if (this.pass2.value.length<8) {
      console.log('su nueva contraseña debe contener almenos 8 caracteres');
      this.presentAlert4();
    }
    else{
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 4,
      Password2: this.pass1.value,
      Password: this.pass2.value,
      idUsuario: this.idUsuario,
    };
    const URL: any = this.baseURL + "perfil.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.cambio = respuesta;
        console.log(this.cambio);
        if (this.cambio == 0) {
          this.presentAlert2();
        } else {
          this.presentAlert3();
        }
      });
    }
  }

  vercontra(event) {
    const val = event.target.value;
    this.pass1.value = val;
    console.log(this.pass1.value);
    if (this.pass1.value === '') {
      this.mostrariconover = false;
    } else {
      this.mostrariconover = true;
    }
  }

  vercontra1(event) {
    const val = event.target.value;
    this.pass2.value = val;
    console.log(this.pass2.value);
    if (this.pass2.value === '') {
      this.mostrariconover1 = false;
    } else {
      this.mostrariconover1 = true;
    }
  }

  vercontra2(event) {
    const val = event.target.value;
    this.pass3.value = val;
    console.log(this.pass3.value);
    if (this.pass3.value === '') {
      this.mostrariconover2 = false;
    } else {
      this.mostrariconover2 = true;
    }
  }

  togglePasswordMode() {
    this.tipodeinput = this.tipodeinput === 'text' ? 'password' : 'text';
    this.iconodeinput = this.iconodeinput === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter() {
    this.mostrariconover = false;
  }

  togglePasswordMode1() {
    this.tipodeinput1 = this.tipodeinput1 === 'text' ? 'password' : 'text';
    this.iconodeinput1 = this.iconodeinput1 === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter1() {
    this.mostrariconover1 = false;
  }

  togglePasswordMode2() {
    this.tipodeinput2 = this.tipodeinput2 === 'text' ? 'password' : 'text';
    this.iconodeinput2 = this.iconodeinput2 === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter2() {
    this.mostrariconover2 = false;
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Su nueva contraseña no coincide',
      buttons: ['Entendido']
    });
    await alert.present();
  }
  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Su contraseña actual no es correcta',
      buttons: ['Entendido']
    });
    await alert.present();
  }
  async presentAlert3() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Su contraseña a sido actualizada',
      buttons: ['Entendido']
    });
    await alert.present();
  }
  async presentAlert4() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Su nueva contraseña debe tener almenos 8 caracteres',
      buttons: ['Entendido']
    });
    await alert.present();
  }
  async presentAlert5() {
    const alert = await this.alertController.create({
      cssClass: 'alertclass',
      header: 'Debe proporcionar su contraseña actual',
      buttons: ['Entendido']
    });
    await alert.present();
  }

  @ViewChild("regForm", { static: false }) myForm: NgForm;
}
