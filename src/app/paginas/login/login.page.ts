import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from '../../services/auth.service';
import { ModalController, NavController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { AlertController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private baseURL = 'https://animatiomx.com/lumi/';

  tipodeinput = 'password';
  iconodeinput = 'eye';

  correo = '';
  contra = '';

  mostrariconover = false;

  constructor(
    private authService: AuthenticationService,
    public modalController: ModalController,
    public toastCtrl: ToastController,
    private alertController: AlertController,
    private http: HttpClient,
    public router: Router,
    public storage: Storage,
    public navCtrl: NavController
  ) {
  }
  datos = {};
  private subscriptions: Subscription[] = [];

  ngOnInit() {
  }

  // metodo ver contraseña

  togglePasswordMode() {
    this.tipodeinput = this.tipodeinput === 'text' ? 'password' : 'text';
    this.iconodeinput = this.iconodeinput === 'eye' ? 'eye-off' : 'eye';
  }

  ionViewWillEnter() {
    this.mostrariconover = false;
  }
  // iniciar seccion

  async login(x) {

    if (x === '' || x === 0) {

      this.presentToast('Favor de verificar que los datos sean correctos');

    } else {
      this.authService.login(x);
      this.correo = '';
      this.contra = '';
    }
  }
  // verificar los datos del formulario

  logForm() {

    if (this.correo === '' && this.contra === '') {

      this.presentToast('El campo usuario y contraseña no deben de ser vacios');

    }
    if (this.correo === '' && this.contra !== '') {

      this.presentToast('El usuario esta vacio');

    }
    if (this.contra === '' && this.correo !== '') {

      this.presentToast('La contraseña esta vacio');

    }
    if (this.correo !== '' && this.contra !== '') {

      // verificar el correo electronico

      const textodeinput = this.correo;
      const emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const verificar = textodeinput.match(emailPattern);
      if (verificar) {
        const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' }),
          options: any = { 'email': this.correo, 'password': this.contra, 'caso': 3 },
          URL: any = this.baseURL + 'acceso.php';
        // this.presentToast( 'Intentando acceder...' );
        // console.log(URL);
        // console.log(JSON.stringify(options));
        // console.log(headers);
        this.subscriptions.push(this.http.post(URL, JSON.stringify(options), headers)
          .subscribe(data => {
            this.presentToast('Bienvenido!');
            console.log(data);
            this.storage.set('idUsuario', data);
            this.storage.set('idL', data);
            localStorage.setItem('idL', data.toString());
            // alert(data.toString());
            this.login(data);
          }));
      } else {
        this.presentToast('Ingrese un correo valido.');
      }
    }

  }

  // metodo para quitar espacios en blanco

  Espaciosdx(event) {
    const contenido = event.target.value;
    event.target.value = contenido.replace('', '');
  }

  // metodo para mandar mensajes como toast

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'bottom',
      cssClass: 'my-custom-class',
    });

    await toast.present();
  }

  // verificar si contraseña esta vacia

  vercontra(event) {
    const val = event.target.value;
    this.contra = val;
    console.log(this.contra);
    if (this.contra === '') {
      this.mostrariconover = false;
    } else {
      this.mostrariconover = true;
    }
  }

}
