import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NuevaCitaoutPage } from './nueva-citaout.page';

const routes: Routes = [
  {
    path: '',
    component: NuevaCitaoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NuevaCitaoutPageRoutingModule {}
