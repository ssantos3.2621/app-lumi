import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NuevaCitaoutPageRoutingModule } from './nueva-citaout-routing.module';

import { NuevaCitaoutPage } from './nueva-citaout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NuevaCitaoutPageRoutingModule
  ],
  declarations: [NuevaCitaoutPage]
})
export class NuevaCitaoutPageModule {}
