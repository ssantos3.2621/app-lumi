import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { LoadingController,ToastController } from '@ionic/angular';
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
import * as moment from 'moment';

@Component({
  selector: 'app-nueva-citaout',
  templateUrl: './nueva-citaout.page.html',
  styleUrls: ['./nueva-citaout.page.scss'],
})
export class NuevaCitaoutPage implements OnInit {
  private baseURL = 'https://animatiomx.com/lumi/';
  prospecto = '';
  idSO = '';
  sucuO: any = [];
  direccion = '';
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  Motivo = '';
  fechainicio;
  fechafinal;
  horainicial;
  horafinal;
  fechayhorainicial;
  fechayhorafinal;
  recordatorioinfO: any = [];
  idrecordatorio = '';
  idempresa = '';
  cprospecto = true;
  store: any;
  empresasinfo: any = [''];
  csuc = true;
  idcita = '';
  nombreE = '';
  checkin = '';
  negado = '';
  idcheck = '';
  fechaelegi: any;
  fechaelegi2: any;
  fecha2: any;
  fecha3: any;
  fecha4: any;
  tiempo: any;

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private subirarchivo: SubirarchivoService,
    private router: Router,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { this.inicializarvariables(); }

  ngOnInit() {
    this.storage.get('idL').then((id) => {
      this.store = id;
      this.idcita = this.route.snapshot.paramMap.get('idC');
      this.nombreE = this.route.snapshot.paramMap.get('NomE');
      this.checkin = this.route.snapshot.paramMap.get('checkI');
      this.idcheck = this.route.snapshot.paramMap.get('idCheck');
      this.recordatorio();
      this.obtenerinfocita();
    });
  }

  obtenerinfocita(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1, 'idCitas': this.idcita };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.prospecto = respuesta[0].SucursalObra;
        this.idempresa = respuesta[0].idEmpresa;
        this.direccion = respuesta[0].Direccion_Empresa;
        if (this.prospecto === '0') {
          this.idSO = '0';
        }else{
          this.idSO = this.prospecto;
        }
        this.obtenSucO(this.idempresa);
      });
  }

  recordatorio(){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 2};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.recordatorioinfO = respuesta;
      });
  }

  obtenSucO(idE) {
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 16, 'idEmp': idE};
    const URL: any = this.baseURL + 'detalle_cita.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.sucuO = respuesta;
        if (this.sucuO != null) {
          this.csuc = true;
        }else{
          this.csuc = false;
        }
      });
  }

  infosuc(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 6, 'sucursal': this.idSO };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.direccion = respuesta[0].direccion;
      });
  }

  fecha(){
    const fecha = new Date();
    const fechahoy = moment(fecha).format();
    const fechaelegida =  moment(this.fechainicio + ' ' + this.horainicial).format();
    console.log(fechahoy);
    console.log(fechaelegida);
    if (fechahoy > fechaelegida) {
      this.fechayhorainicial = '';
    } else {
      this.fechayhorainicial = (moment(this.fechainicio).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horainicial;
      this.fechaelegi = this.fechainicio.concat('T' + this.horainicial.toString());
      this.fechaelegi2 = new Date(this.fechaelegi);
    // Tiempo es igual a un dia en microsecs
      if (this.idrecordatorio === '1')
    {
      this.tiempo = 1000 *  60 * 5;
    }
      if (this.idrecordatorio === '2')
    {
      this.tiempo = 1000 *  60 * 15;
    }
      if (this.idrecordatorio === '3')
    {
      this.tiempo = 1000 *  60 * 30;
    }
      if (this.idrecordatorio === '4')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 1;
    }
      if (this.idrecordatorio === '5')
    {
      this.tiempo = 1000 *  60 * 60 * 24 * 2;
    }
    // getTime convierte una fecha a microsegundos
      this.fecha2 = this.fechaelegi2.getTime();
    // Operacion para quitar el tiempo a la fecha asignada
      this.fecha3 = this.fecha2 - this.tiempo;
      console.log(this.fechaelegi);
      console.log(this.fechaelegi2);
      console.log(this.fecha2 + 'Esta es fechat 2 getTime');
    // convirtiendo a date los microsegundis nos da una fecha
      console.log(new Date(this.fecha3) + 'Esta es fechat 3 en date');
    }
    this.fecha1();
  }

  async noti(id,f,e) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title: "Recordatorio de tu cita",
          body: "Tienes una cita asignada el día de hoy",
          id: id,
          schedule: { at: new Date(this.fecha3 ) },
          sound: null,
          attachments: null,
          extra: {
            route: ['/tabs/tab1/mapa-cita/'+String(f)+'/'+e+'/'+id],
          },
          iconColor: '#BD008C',
        }
      ]
    });
    console.log('scheduled notifications', notifs);
    console.log(new Date());
    console.log('Se ha guardado tu notificacion con el id: ' + id);
  }

  fecha1(){
    const fechainicio =  moment(this.fechainicio + ' ' + this.horainicial).format();
    const fechaelegida =  moment(this.fechafinal + ' ' + this.horafinal).format();
    if (fechainicio > fechaelegida) {
      this.fechayhorafinal = '';
    } else {
      this.fechayhorafinal = (moment(this.fechafinal).format('dddd DD [de] MMMM [de] YYYY')) + ' ' + this.horafinal;
    }
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  async crearcita(){
    let loader =await this.loadingCtrl.create({
      message: "Creando cita"
    });
    await loader.present();
    // console.log('direccion '+ this.direccion + 'Motivo '+ this.Motivo + 'fecha i'+ this.fechainicio+
    // ' hora i '+ this.horainicial+ 'fecha f'+ this.fechafinal + ' hora f' + this.horafinal+
    // 'usuario'+ this.store + 'recordatorio'+ this.idrecordatorio+ 'empresa' + this.idempresa+'sucursal' + this.idSO);
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 10,'direccion':this.direccion,'motivo':this.Motivo, 'fechai':this.fechainicio,
    'fechaf':this.fechafinal,'horai':this.horainicial,'horaf':this.horafinal,'idSO': this.idSO,'usuario':this.store,
    'recordatorio':this.idrecordatorio, 'empresa': this.idempresa, 'idcheck': this.idcheck};
    let fechaInicial = this.fechainicio.concat(this.horainicial.toString());
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {

        const idcita = respuesta.toString();

        if (this.archivos.length === 0) {
          this.noti(idcita, fechaInicial, this.nombreE);
          loader.dismiss();
          this.inicializarvariables();
          this.checkout();
          this.mensaje('Cita creada correctamente');
        } else {
          const formData = new FormData();
          for (var i = 0; i < this.archivos.length; i++) {
            formData.append("file[]", this.archivos[i]);
          }
          formData.append('idcita',idcita);
          formData.append('idtarea','0');
          formData.append('idEmp', this.idempresa);

          this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
            if (resp.toString() !== "") {
              this.noti(idcita, fechaInicial, this.nombreE);
              loader.dismiss();
              this.inicializarvariables();
              this.checkout();
              this.mensaje('Cita creada correctamente');
            } else {
              loader.dismiss();
              this.mensaje('ocurrio un error intente nuevamente');
            }
          });
        }
      });
  }

  async mensaje(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000,
      cssClass: "toastcurva",
    });

    await toast.present();

}

inicializarvariables(){
  this.nombredearchivos = [];
  this.archivos = [];
  this.tipodearchivo = [];
}

checkout(){
  this.router.navigate(['/tabs/tab1/chek-out/'+ this.idcita + '/'  + this.checkin + '/' + this.nombreE + '/' + this.idcheck]);
}
}
