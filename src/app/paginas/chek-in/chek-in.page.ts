import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SubirarchivoService } from 'src/app/services/subirarchivo.service';
import { LoadingController,ToastController } from '@ionic/angular';

@Component({
  selector: 'app-chek-in',
  templateUrl: './chek-in.page.html',
  styleUrls: ['./chek-in.page.scss'],
})
export class ChekInPage implements OnInit {
  private baseURL = 'https://animatiomx.com/lumi/';
  idcita = '';
  infocita: any = [];
  usuariosE : any = [];
  todosusuarios : any = [];
  asesor = '';
  archivos: string[] = [];
  nombredearchivos: any = [];
  tipodearchivo: any = [];
  usuario = '';
  tarea = '';
  sucursal = '';
  idempresa = '';
  nota = '';
  nombreempresa = '';
  checkin = '';
  idcheckin = '';
  tareas: any = [];
  tareas1: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient,
    private subirarchivo: SubirarchivoService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { this.inicializarvariables(); }

  ngOnInit() {
    this.idcita = this.route.snapshot.paramMap.get('idC');
    this.obtenerinfocita();
  }

  obtenerinfocita(){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1, 'idCitas': this.idcita };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.infocita = respuesta;
        this.usuario = this.infocita[0].Users_idUsers;
        const eqp = this.infocita[0].Equipos_idEquipos;
        this.sucursal = this.infocita[0].SucursalObra;
        this.idempresa = this.infocita[0].idEmpresa;
        this.nombreempresa = this.infocita[0].Nombre_Empresa;
        this.checkin = this.infocita[0].HoraInicio;
        this.idcheckin = this.infocita[0].idChecks;
        this.usuariosequipo(this.usuario,eqp);
        this.todoslosusuarios(eqp);
        this.historialtareas(this.idcheckin);

      });
  }

  historialtareas(idCheck){
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 8, 'idC': idCheck };
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.tareas1 = respuesta;
        if (this.tareas1.length > 0) {
          this.tareas = respuesta;
        }

      });
  }

  usuariosequipo(usuario,et){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 4, 'EquipoT': et, 'usuario': usuario};
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.usuariosE = respuesta;
      });
  }

  todoslosusuarios(et){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 2, 'EquipoT': et};
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.todosusuarios = respuesta;
      });
  }

  onFileChanged(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      this.archivos.push(event.target.files[i]);
      this.nombredearchivos.push(event.target.files[i].name);
      var ext = event.target.files[i].name
        .substring(event.target.files[i].name.lastIndexOf(".") + 1)
        .toLowerCase();
      this.tipodearchivo.push(ext);
    }
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  quitararchivo(i) {
    this.archivos.splice(i, 1);
    this.nombredearchivos.splice(i, 1);
    this.tipodearchivo.splice(i, 1);
    console.log(this.nombredearchivos);
    console.log(this.archivos);
    console.log(this.tipodearchivo);
  }

  comprobardatos(){
    if (this.tarea !=='' && this.asesor !=='') {
      this.guardartarea();
    } else {
      if (this.tarea ==='' && this.asesor ==='') {
        this.mensaje('Debes escribir la tarea y escoger un asesor');
      }
      if (this.tarea ==='' && this.asesor !=='') {
        this.mensaje('Debes escribir la tarea');
      }
      if (this.tarea !=='' && this.asesor ==='') {
        this.mensaje('Debes escoger a un asesor');
      }
    }
  }

  async guardartarea() {
    let loader =await this.loadingCtrl.create({
      message: "Creando tarea"
    });
    await loader.present();
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 3, 'tarea': this.tarea, 'usuario': this.usuario, 'responsable': this.asesor,
     'sucursal': this.sucursal, 'empresa': this.idempresa, 'nota' : this.nota,'idC':this.idcheckin};
    const URL: any = this.baseURL + 'check.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        const idtarea = respuesta.toString();
        if (this.archivos.length === 0) {
          loader.dismiss();
          this.mensaje('Tarea enviada correctamente');
          this.historialtareas(this.idcheckin);
          this.inicializarvariables();
        } else {
        const formData = new FormData();
        for (var i = 0; i < this.archivos.length; i++) {
          formData.append("file[]", this.archivos[i]);
        }
        formData.append("idtarea", idtarea);
        formData.append("idEmp", this.idempresa);
        formData.append("idcita", "0");

        this.subirarchivo.enviararchivo(formData).subscribe((resp) => {
          if (resp.toString() !== "") {
            loader.dismiss();
            this.mensaje('Tarea enviada correctamente');
            this.historialtareas(this.idcheckin);
            this.inicializarvariables();
          } else {
            loader.dismiss();
            this.mensaje('ocurrio un error intente nuevamente');
          }
        });
        }

      });
  }

  async mensaje(msg) {
      let toast = await this.toastCtrl.create({
        message: msg,
        duration: 2000,
        cssClass: "toastcurva",
      });

      await toast.present();

  }

  inicializarvariables(){
    this.nombredearchivos = [];
    this.archivos = [];
    this.asesor = '';
    this.tipodearchivo = [];
    this.tarea = '';
    this.nota = '';
  }

  checkout(){
    this.router.navigate(['/tabs/tab1/chek-out/'+ this.idcita + '/'  + this.checkin + '/' + this.nombreempresa + '/' + this.idcheckin]);
  }


  async mensaje1(asesor,tarea) {
    let toast = await this.toastCtrl.create({
      message: 'Asesor: '+asesor+'\nTarea: '+tarea,
      duration: 3000,
      position: 'top',
      cssClass: "toastcurva",
    });

    await toast.present();

}

}
