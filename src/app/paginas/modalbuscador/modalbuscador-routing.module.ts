import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalbuscadorPage } from './modalbuscador.page';

const routes: Routes = [
  {
    path: '',
    component: ModalbuscadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalbuscadorPageRoutingModule {}
