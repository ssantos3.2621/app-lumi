import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalbuscador',
  templateUrl: './modalbuscador.page.html',
  styleUrls: ['./modalbuscador.page.scss'],
})
export class ModalbuscadorPage implements OnInit {

  private baseURL = 'https://animatiomx.com/lumi/';
  arreglobuscador:any;
  letrabuscador = '';
  empresasid: any = [];
  @Input() Titulo;
  @Input() Accion;
  @Input() id;
  @Input() nombre;
  textobuscar = '';

  constructor(
    private http: HttpClient, 
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.letrabuscador = "";
  }

  empresas(buscar){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 24, 'buscador':buscar};
    const URL: any = this.baseURL + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasid = respuesta;
      });
  }

  buscador(ev: any) {
    this.initializeItems();
    const val = ev.target.value;
    this.letrabuscador = val;
    this.letrabuscador = val;
    if (this.letrabuscador && this.letrabuscador.trim() != '') {
    this.arreglobuscador = this.arreglobuscador.filter((item) => {
      return (item.Categoria.toLowerCase().indexOf(this.letrabuscador.toLowerCase()) > -1);
    })
    }
  }

  initializeItems(){ 
    this.arreglobuscador = this.empresasid;
  }

  comeback(id,nombre) {
    if (id === '' || nombre === '') {
      if (this.id === '' || nombre === '') {
        this.modalController.dismiss({cierra: id,nombreempresa:nombre});
      } else {
        this.modalController.dismiss({cierra: this.id,nombreempresa:this.nombre});
      }
    }else{
      this.modalController.dismiss({cierra: id,nombreempresa:nombre});
    }
  }

  buscarempresa(){

  }

}
