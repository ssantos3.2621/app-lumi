import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalbuscadorPageRoutingModule } from './modalbuscador-routing.module';

import { ModalbuscadorPage } from './modalbuscador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalbuscadorPageRoutingModule
  ],
  declarations: [ModalbuscadorPage]
})
export class ModalbuscadorPageModule {}
