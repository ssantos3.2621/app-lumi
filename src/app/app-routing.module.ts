import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./paginas/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'recuperar-contra',
    loadChildren: () => import('./paginas/recuperar-contra/recuperar-contra.module').then( m => m.RecuperarContraPageModule)
  },  {
    path: 'modalbuscador',
    loadChildren: () => import('./paginas/modalbuscador/modalbuscador.module').then( m => m.ModalbuscadorPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
