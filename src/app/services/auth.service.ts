import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = '';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  Authenticationstate = new BehaviorSubject(false);
  constructor(private storage: Storage, private  plt: Platform) {
    this.plt.ready().then(() => {      this.checkToken();
    });
  }
  login(x) {
      localStorage.setItem('idu', x);
    return this.storage.set(TOKEN_KEY, x).then(res => { this.Authenticationstate.next(true);
      });
  }
  logout() {
    return this.storage.remove(TOKEN_KEY).then(res => { this.Authenticationstate.next(false);
      localStorage.removeItem('idUsuario');
      localStorage.removeItem('idu');
      localStorage.removeItem('idL');
      localStorage.clear();
      });
  }
  isAutentication() {
    return this.Authenticationstate.value;
  }
  checkToken() {
    return this.storage.get(TOKEN_KEY).then(res => {
        if (res) { this.Authenticationstate.next(true); }
      });
  }
}

