import { Component, OnInit, ɵConsole, ViewChild } from "@angular/core";
import { HttpHeaders } from "@angular/common/http";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Params, Router } from "@angular/router";
import * as moment from 'moment'
import { Plugins, LocalNotification, LocalNotificationActionPerformed } from '@capacitor/core';
const { LocalNotifications } = Plugins;
@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"],
})
export class Tab2Page {
  private baseURL = "https://animatiomx.com/lumi_app/";
  private baseURL2 = "https://animatiomx.com/lumi/";
  tarea: any = [];
  empresasid1: any = [];
  idUsuario;
  idemp = "";
  fecha = "";
  fecha2 = "";
  fecha3 = "";
  fecha4 = "";
  info = [];
  pendiente="pendiente";
  filtrofecha="";
  filtroperson="";
  filtroemp="";
  usuario: any = [];

  @ViewChild("fechaselect", { static: false }) fechaselect: any;
  @ViewChild("personselect", { static: false }) personselect: any;
  @ViewChild("statuselect", { static: false }) statuselect: any;
  @ViewChild("empselect", { static: false }) empselect: any;

  constructor(
    public route: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {
    const info1 = localStorage.getItem("idL");
    const info2 = info1.replace("[", "").replace("]", "").replace("", "");
    this.info = info2.split(",");
    this.idUsuario = this.info[0].replace('"', "").replace('"', "");
    console.log(this.idUsuario);
  }

  ngOnInit() {
    this.fecha = moment().format("YYYY-MM-DD");
    this.fecha2 = moment().subtract(1, "months").format("YYYY-MM-DD");
    this.fecha3 = moment().subtract(7, "d").format("YYYY-MM-DD");
    this.fecha4 = moment().subtract(2, "months").format("YYYY-MM-DD");
    this.obtenerempresas();
    this.infouser(this.idUsuario);
    LocalNotifications.requestPermission();
    // LocalNotifications.addListener('localNotificationReceived', (notification) => {
    //   console.log('Notification: ', notification);
    //   this.presentAlert(1, 2);
    // });
    // LocalNotifications.addListener('localNotificationReceived', (notification: LocalNotification) => {
    //   this.presentAlert(`${notification.title}`, `${JSON.stringify(notification.extra)}`);
    //   this.cosa1 = `${notification.title}`;
    //   this.cosa2 = `${JSON.stringify(notification.extra)}`;
    //   console.log(`${notification.title}`, `${JSON.stringify(notification.extra)}`, 'hola');
    // });
    LocalNotifications.addListener('localNotificationActionPerformed', (payload) => {
      console.log(payload.actionId);
      const route = payload.notification.extra.route;
      this.router.navigate(route);
      // this.obtenerifocita(payload.actionId);
});
this.deshabilitaRetroceso();
  }

  deshabilitaRetroceso() {
    window.location.hash = 'no-back-button';
    window.location.hash = 'Again-No-back-button' //chrome
    window.onhashchange = function () { window.location.hash = 'no-back-button'; }
  }

  ionViewWillEnter() {
    this.ngOnInit();
    this.obtenertarea1();
    this.pendiente = "pendiente";
  }

  infouser(id){
    const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
    const options: any = { 'caso': 0, 'idUsuario': id};
    const URL: any = this.baseURL2 + 'perfil.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.usuario = respuesta;
        const rol = this.usuario[0].Rol;
        if (rol === '0') {
          console.log('si')
          this.router.navigate(['/tabs/tab3']);
        } else {
          console.log('no')
        }
      });
  }

  obtenerempresas() {
    const headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    const options: any = { 'caso': 1, 'idUsuario': this.idUsuario };
    const URL: any = this.baseURL2 + 'calendario.php';
    this.http.post(URL, JSON.stringify(options), headers).subscribe(
      respuesta => {
        this.empresasid1 = respuesta;
      });
  }

  obtenertarea() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0.1, 'idU':this.idUsuario};
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
      });
  }

  obtenertarea1() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 0, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.filtrofecha = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenertarea2() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 8, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.filtrofecha = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }
  obtenertarea3() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 9, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.filtrofecha = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenerempresa(idemp) {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 12.1, 'idUs': this.idUsuario, 'idEmp': idemp};
    const URL: any = this.baseURL2 + "detalle_cita1.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtrofecha = "";
        this.filtroperson = "";
      });
  }

  obtenertareahoy() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 10, fecha1: this.fecha, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenertareames() {
    console.log(this.fecha);
    console.log(this.fecha2);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 11,
      fecha1: this.fecha,
      fecha2: this.fecha2,
      idU: this.idUsuario,
    };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenertarea2meses() {
    console.log(this.fecha);
    console.log(this.fecha4);
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 11,
      fecha1: this.fecha,
      fecha2: this.fecha4,
      idU: this.idUsuario,
    };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenertareasemana() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = {
      caso: 11,
      fecha1: this.fecha,
      fecha2: this.fecha3,
      idU: this.idUsuario,
    };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtroperson = "";
        this.filtroemp = "";
      });
  }

  obtenertareaparami() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 12, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtrofecha = "";
        this.filtroemp = "";
      });
  }

  obtenertareaparaotro() {
    const headers: any = new HttpHeaders({
      "Content-Type": "application/json",
    });
    const options: any = { caso: 13, idU: this.idUsuario };
    const URL: any = this.baseURL + "tareas.php";
    this.http
      .post(URL, JSON.stringify(options), headers)
      .subscribe((respuesta) => {
        this.tarea = respuesta;
        if (this.tarea == null) {
          this.tarea = [];
        }
        console.log(this.tarea);
        this.pendiente = "";
        this.filtrofecha = "";
        this.filtroemp = "";
      });
  }

  public fechas(event: any): void {
    if (this.filtrofecha == "hoy") {
      this.obtenertareahoy();
    }
    if (this.filtrofecha == "semana") {
      this.obtenertareasemana();
    }
    if (this.filtrofecha == "mes") {
      this.obtenertareames();
    }
    if (this.filtrofecha == "meses") {
      this.obtenertarea2meses();
    }
    if (this.filtrofecha == "todos") {
      this.obtenertarea();
    }

    console.log(this.filtrofecha);
  }

  public person(event: any): void {
    if (this.filtroperson == "mi") {
      this.obtenertareaparami();
    }
    if (this.filtroperson == "otro") {
      this.obtenertareaparaotro();
    }
    if (this.filtroperson == "todos") {
      this.obtenertarea();
    }

    console.log(this.filtroperson);
  }

  public status(event: any): void {
    if (this.pendiente == "pendiente") {
      this.obtenertarea1();
    }
    if (this.pendiente == "finalizada") {
      this.obtenertarea2();

    }
    if (this.pendiente == "cancelada") {
      this.obtenertarea3();

    }
    if (this.pendiente == "todos") {
      this.obtenertarea();
    }
    console.log(this.pendiente);
  }

  public empresas(event: any): void {
    this.idemp = this.empselect.value;
    console.log(this.idemp);
    if (this.filtroemp == "todos") {
      this.obtenertarea();
    } else {
      this.obtenerempresa(this.idemp);
    }
  }
}
