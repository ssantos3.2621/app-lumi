import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuard } from '../services/auth.guard';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          },
          {
            path: 'nueva-cita',
            loadChildren: () => import('../paginas/nueva-cita/nueva-cita.module').then(m => m.NuevaCitaPageModule)
          },
          {
            path: 'nueva-citaout/:idC/:checkI/:NomE/:idCheck',
            loadChildren: () => import('../paginas/nueva-citaout/nueva-citaout.module').then( m => m.NuevaCitaoutPageModule)
          },
          {
            path: 'detalle-cita/:idC',
            loadChildren: () => import('../paginas/detalle-cita/detalle-cita.module').then(m => m.DetalleCitaPageModule)
          },
          {
            path: 'chek-in/:idC',
            loadChildren: () => import('../paginas/chek-in/chek-in.module').then(m => m.ChekInPageModule)
          },
          {
            path: 'chek-out/:idC/:checkI/:NomE/:idCheck',
            loadChildren: () => import('../paginas/chek-out/chek-out.module').then(m => m.ChekOutPageModule)
          },
          {
            path: 'mapa-cita/:fc/:nemp/:idc',
            loadChildren: () => import('../paginas/mapa-cita/mapa-cita.module').then(m => m.MapaCitaPageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          },
          {
            path: 'nueva-tarea',
            loadChildren: () => import('../paginas/nueva-tarea/nueva-tarea.module').then(m => m.NuevaTareaPageModule)
          },
          {
            path: 'detalle-tarea/:idT',
            loadChildren: () => import('../paginas/detalle-tarea/detalle-tarea.module').then(m => m.DetalleTareaPageModule)
          },
          {
            path: 'nueva-citatar/:idu/:ide/:ids',
            loadChildren: () => import('../paginas/nueva-citatar/nueva-citatar.module').then(m => m.NuevaCitatarPageModule)
          },
        ]
      },
      {
        path: 'tab3',
        loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'tab4',
        children: [
          {
            path: '',
            loadChildren: () => import('../tab4/tab4.module').then(m => m.Tab4PageModule)
          },
          {
            path: 'cambiar-contra/:id',
            loadChildren: () => import('../paginas/cambiar-contra/cambiar-contra.module').then(m => m.CambiarContraPageModule)
          },
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
