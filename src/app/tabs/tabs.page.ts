import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {

private baseURL = 'https://animatiomx.com/lumi/';
usuario: any = [];
rol = '';

constructor(private http: HttpClient,private storage: Storage) {

  }

ngOnInit(){
  this.storage.get('idL').then((id) => {
    this.infouser(id);
  });
}

infouser(id){
  const headers: any = new HttpHeaders({'Content-Type' : 'application/json'});
  const options: any = { 'caso': 0, 'idUsuario': id};
  const URL: any = this.baseURL + 'perfil.php';
  this.http.post(URL, JSON.stringify(options), headers).subscribe(
    respuesta => {
      this.usuario = respuesta;
      this.rol = this.usuario[0].Rol;
    });
}

}
